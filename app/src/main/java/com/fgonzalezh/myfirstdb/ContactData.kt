package com.fgonzalezh.myfirstdb

data class ContactData (
    val name: String,
    val lastName: String,
    val age: Int
)